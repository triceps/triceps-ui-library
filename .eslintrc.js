// eslint-disable-next-line no-undef
module.exports = {
  'env': {
    'browser': true,
    'es6': true
  },
  'extends': [
    'eslint:recommended',
    'plugin:react/recommended'
  ],
  'parserOptions': {
    'ecmaFeatures': {
      'jsx': true
    },
    'ecmaVersion': 12,
    'sourceType': 'module'
  },
  'plugins': [
    'react'
  ],
  'rules': {
    // we only want single quotes
    'quotes': ['error', 'single'],
    // we want to force semicolons
    'semi': ['error', 'never'],
    // we use 2 spaces to indent our code
    'indent': ['error', 2, {
      'ignoredNodes': ['TemplateLiteral']
    }],
    // we want to avoid useless spaces
    'no-multi-spaces': ['error'],
    'object-curly-spacing': ['error', 'always'],
    'react/prop-types': ['warn']
  },
  'overrides': [
    {
      'files': [
        '**/*.test.js',
        '**/*.test.jsx'
      ],
      'env': {
        'jest': true
      }
    }
  ]
}
