import React from 'react'
import { render, screen } from '@testing-library/react'

import { Button } from './Button'

describe('<Button />', () => {
  test('rendered label', () => {
    render(<Button label="sample Button"/>)
    expect(screen.getByText('sample Button')).toBeDefined()
  })
})