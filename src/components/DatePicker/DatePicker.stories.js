// DatePicker.stories.js|jsx

import React from 'react'

import { DatePicker } from './DatePicker'

export default {
  title: 'DatePicker',
  component: DatePicker,
}

//👇 We create a “template” of how args map to rendering
const Template = (args) => <DatePicker {...args} />

// 👇 Each story then reuses that template
export const Primary = Template.bind({})