import React from 'react'
import PropTypes from 'prop-types'
import { DatePicker as AntDatePicker } from 'antd'
import locale from 'antd/locale/es_ES'

export const DatePicker = ({
//   value = new Date(),
  dateFormat = 'DD-MM-YYYY',
  ...rest 
}) => {
  return (
    <AntDatePicker 
      {...rest}
      //   value={value}
      locale={locale}
      format={dateFormat} 
    />
  )
}

DatePicker.propTypes = {
  value: PropTypes.instanceOf(Date),
  dateFormat: PropTypes.string
}
