// DatePicker.stories.js|jsx

import React from 'react'

import { Empty } from './Empty'

export default {
  title: 'Empty',
  component: Empty,
}

//👇 We create a “template” of how args map to rendering
const Template = (args) => <Empty {...args} />

// 👇 Each story then reuses that template
export const Primary = Template.bind({})